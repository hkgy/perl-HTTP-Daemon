Name:           perl-HTTP-Daemon
Version:        6.14
Release:        1
Summary:        A simple http server class
License:        Artistic-1.0 or GPL-1.0+
Url:            http://search.cpan.org/dist/HTTP-Daemon/
Source:         http://www.cpan.org/authors/id/O/OA/OALDERS/HTTP-Daemon-6.14.tar.gz
BuildArch:      noarch

BuildRequires:  perl(Test::More)
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(HTTP::Request)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(HTTP::Tiny) >= 0.042
BuildRequires:  perl(Module::Metadata)
BuildRequires:  perl(Test::Needs)

Requires:       perl(HTTP::Date) >= 6
Requires:       perl(HTTP::Response) >= 6
Requires:       perl(HTTP::Status) >= 6
Requires:       perl(IO::Socket)
Requires:       perl(LWP::MediaTypes) >= 6
Requires:       perl
Requires:       perl(IO::Socket::IP) >= 0.25


%description
Instances of the `HTTP::Daemon' class are HTTP/1.1 servers that listen
on a socket for incoming requests. The `HTTP::Daemon' is a subclass of
`IO::Socket::INET', so you can perform socket operations directly on it
too.

The accept() method will return when a connection from a client is
available. The returned value will be an `HTTP::Daemon::ClientConn'
object which is another `IO::Socket::INET' subclass. Calling the
get_request() method on this object will read data from the client and
return an `HTTP::Request' object. The ClientConn object also provide
methods to send back various responses.

This HTTP daemon does not fork(2) for you. Your application, i.e. the
user of the `HTTP::Daemon' is responsible for forking if that is
desirable. Also note that the user is responsible for generating
responses that conform to the HTTP/1.1 protocol.

%package  help
Summary:        Development support for perl-HTTP-Daemon

%description    help
Development headers and libraries for perl-HTTP-Daemon.

%prep
%autosetup -n HTTP-Daemon-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%make_build

%install
%{make_install}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENCE
%defattr(-,root,root,755)
%doc Changes CONTRIBUTING README
%{perl_vendorlib}/*

%files help
%doc Changes CONTRIBUTING README
%{_mandir}/man3/*

%changelog
* Fri Apr 29 2022 hkgy <kaguyahatu@outlook.com> - 6.14-1
- Upgrade version to 6.14

* Wed Jul 23 2020 xinghe <xinghe1@huawei.com> - 6.12-1
- update version to 6.12

* Mon Jan 6 2020 duyeyu <duyeyu@huawei.com> - 6.01-26
- Package init
